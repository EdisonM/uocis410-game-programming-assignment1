using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class Player_Controller : MonoBehaviour
{

    private Vector3 jump;
    private bool doubleJump;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    public float jumpHeight = 2.0f;
    public float speed = 0;
    public float sprintMult = 2.0f;
    private Rigidbody rb;
    private float movementX;
    private float movementY;
    private int count;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 2.0f, 0.0f);

        count = 0;

        SetCountText ();

        winTextObject.SetActive(false);


    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void OnCollisionStay()
    {
        doubleJump = true;
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        //rb.AddForce(movement * speed);
        if (Input.GetKey(KeyCode.LeftShift))
        {
            rb.AddForce((movement * speed) * sprintMult);
        }
        else
        {
            rb.AddForce(movement * speed);
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && doubleJump)
        {
            rb.AddForce(jump * jumpHeight, ForceMode.Impulse);
            doubleJump = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);

            count = count + 1;

            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 18)
        {
            winTextObject.SetActive(true);
        }
    }
}
